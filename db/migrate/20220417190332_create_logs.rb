class CreateLogs < ActiveRecord::Migration[6.1]
  def change
    create_table :logs do |t|
      t.references :material_stock, null: false, type: :integer, foreign_key: { on_update: :cascade, on_delete: :cascade }
      t.references :user, null: false, type: :integer, foreign_key: { on_update: :cascade, on_delete: :cascade }
      t.integer :quantity
      t.string :action
      t.timestamps
    end
  end
end
