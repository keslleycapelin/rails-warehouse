class CreateMaterialStock < ActiveRecord::Migration[6.1]
  def change
    create_table :material_stocks do |t|
      t.integer :quantity
      t.references :material, null: false, type: :integer, foreign_key: { on_update: :cascade, on_delete: :cascade }
      t.timestamps
    end
  end
end
