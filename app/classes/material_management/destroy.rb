# frozen_string_literal: true

module MaterialManagement
  class Destroy < CRUDManagement::Destroy
    validate :validate_material_stock, if: :instance

    def destroy
      @instance = load_object

      if valid?
        instance.delete
        callbacks[:success]&.call(instance)
      else
        callbacks[:error]&.call(self)
      end
    end

    private

    def validate_material_stock
      return if instance&.material_stock.nil?

      errors.add(:base, "você não pode excluir este material")
    end

  end
end