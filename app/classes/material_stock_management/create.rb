# frozen_string_literal: true

module MaterialStockManagement
  class Create < CRUDManagement::Create
    attr_reader :flag, :material_id, :quantity, :current_user_id

    def initialize(params, callbacks, klass)
      @flag = params.delete(:flag)
      @material_id = params.delete(:material_id)
      @quantity = params.delete(:quantity)
      @current_user_id = params.delete(:current_user_id)
      super
    end

    def create
      @instance = create_or_update
      if valid?
        begin
          instance.transaction do
            movement
            instance.save!
            callbacks[:success]&.call(instance)
          end
        rescue StandardError => e
          Rails.logger.error("An error occurred while sending a new verification code: #{e.full_message}")
          instance.errors.add(:base, I18n.t('api.errors.create_material_stock'))
          callbacks[:error]&.call(instance)
        end
      else
        callbacks[:error]&.call(self)
      end
    end

    private

    def create_or_update
      MaterialStock.find_or_create_by(material_id: material_id)
    end

    def movement
      case flag
      when Movement::ADD
        instance.quantity = instance.quantity.to_i + quantity.to_i
        create_log(flag)
      when Movement::REMOVE
        validate_removal
      end 
    end

    def validate_removal
      @day = DateTime.current
      if quantity.to_i > instance.quantity.to_i
        errors.add(:base, I18n.t('api.errors.quantity_invalid'))
      else
        if business_hours? && weekend?
          instance.quantity = instance.quantity.to_i - quantity.to_i
          create_log(flag)
        else
          errors.add(:base, I18n.t('api.errors.remove_material_stock'))
        end
      end
    end

    def create_log(action)
      Log.create(action: action, material_stock_id: instance.id, user_id: current_user_id, quantity: quantity)
    end

    def weekend?
       @day.sunday? || @day.saturday?
    end
    
    def business_hours?
      @day.hour >= 9 && @day.hour <= 18
    end
  end
end
