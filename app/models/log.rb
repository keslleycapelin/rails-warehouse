class Log < ApplicationRecord
  belongs_to :material_stock
  belongs_to :user

  validates :action, presence: true

  has_enumeration_for :action, with: Movement, create_scopes: true, create_helpers: true
end