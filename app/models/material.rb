class Material < ApplicationRecord
  has_one :material_stock
  validates :name, presence: true, uniqueness: true

end