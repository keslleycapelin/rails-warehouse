class MaterialStock < ApplicationRecord
  belongs_to :material
  validates :material, uniqueness: { scope: :material_id}
end