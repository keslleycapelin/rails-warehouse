module ApplicationHelper
  def error_per_field(object, field)
    return if object.errors.blank?

    object.errors.filter { |e| e.attribute.eql?(field.to_sym) }.first&.full_message
  end

  def flash_message_classes(flash_type, message)
    {
      notice: message,
      error: message,
      alert: message
    }.stringify_keys[flash_type.to_s] || flash_type.to_s
  end
end