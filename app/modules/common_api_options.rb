module CommonApiOptions
  def search_params
    params.permit(q: {})
  end

  def sort_options
    sort_params = params.permit(:sort)
    sort_params.transform_keys { |_k| :s }
  end


  def ransack_options
    (search_params[:q] || {}).merge(sort_options)
  end

  def default_index_callbacks
    {success: success_callback_default}
  end

  private 

  def success_callback_default
    lambda do |caller|
      @items = caller
    end
  end
end