class Movement < EnumerateIt::Base
  associate_values(
    :add,
    :remove
  )
end