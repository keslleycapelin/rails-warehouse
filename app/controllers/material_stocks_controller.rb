class MaterialStocksController < ApplicationController
  def new
    @material_id = material_stock_params[:material_id]
    @flag = material_stock_params[:flag]
    material_stock = MaterialStock.new
  end
  
  def index
    CRUDManagement::Index.perform(
      paginate_params.merge(ransack_params: ransack_options),
      { success: success_callbacks, error: error_callbacks },
      Material
    )
  end

  def show

  end

  def create
    MaterialStockManagement::Create.perform(
      material_stock_params.merge(quantity: params[:quantity], current_user_id: session[:user_id]),
      { success: success_callbacks, error: error_callbacks },
      MaterialStock
    )
  end

  def update

  end

  def destroy
    MaterialManagement::Destroy.perform(
      find_params,
      { success: success_callbacks, error: error_callbacks },
      Material
    )
  end

  private

  def success_callbacks
    lambda do |caller|
      @items = caller
      redirect_to materials_path
    end
  end

  def error_callbacks
    lambda do |caller|
      flash[:alert] = caller.errors.errors.last.type
      redirect_to materials_path
    end
  end

  def material_stock_params
    params.require(:material_stock).permit(:id, :material_id, :quantity, :flag)
  end


  def find_params
    { id: params.require(:id) }
  end 
end