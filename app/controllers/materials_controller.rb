class MaterialsController < ApplicationController
  def new
    @user = Material.new
  end
  
  def index
    CRUDManagement::Index.perform(
      paginate_params.merge(ransack_params: ransack_options),
      { success: success_callbacks, error: error_callbacks },
      search.present? ? Material.where(name: search) : Material
    )
  end

  def create
    success_callbacks = lambda do |caller|
      @items = caller
      redirect_to materials_path
    end
    CRUDManagement::Create.perform(
      material_params,
      { success: success_callbacks, error: error_callbacks },
      Material
    )
  end

  def edit
    @material = Material.find_by(find_params)
  end


  def update
    success_callbacks = lambda do |caller|
      redirect_to materials_path
    end

    CRUDManagement::Update.perform(
      find_params,
      material_update_params,
      { success: success_callbacks, error: error_callbacks },
      Material
    )
  end

  def destroy
    MaterialManagement::Destroy.perform(
      find_params,
      { success: success_callbacks, error: error_callbacks },
      Material
    )
  end

  private

  def success_callbacks
    lambda do |caller|
      @items = caller
    end
  end

  def error_callbacks
    lambda do |caller|
      flash[:alert] = caller.errors.errors.last.type
    end
  end

  def material_params
    params.permit(:name)
  end

  def material_update_params
    params.require(:material).permit(:name)
  end

  def search
    params[:search]
  end

  def find_params
    { id: params.require(:id) }
  end 
end