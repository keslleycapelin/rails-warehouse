class SessionsController < ApplicationController

  def new
    session[:user_id] = nil
  end

  def create
    user = User.find_by(username: params[:session][:username].downcase)
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      flash[:notice] = "Logado com sucesso."
      redirect_to materials_path
    else
      flash.now[:alert] = "Problemas na autenticação."
      render 'new'
    end
  end
   
  def destroy
    session[:user_id] = nil
    flash[:notice] = "You have been logged out."
    redirect_to login_path
  end
end