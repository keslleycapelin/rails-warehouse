class LogsController < ApplicationController
  def index
    CRUDManagement::Index.perform(
      paginate_params.merge(ransack_params: ransack_options),
      default_index_callbacks,
      scope
    )
  end

  private

  def scope
    Log.joins(:material_stock).where(material_stocks: {material_id: material_id})
  end

  def material_id
    params.require(:log).permit(:material_id)[:material_id]
  end 
end