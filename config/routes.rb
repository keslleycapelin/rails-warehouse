Rails.application.routes.draw do
  root 'welcome#index'

  get 'welcome/index'
  get "signup", to: "users#new"
  get "login", to: "sessions#new"
  post "login", to: "sessions#create"
  delete "logout", to: "sessions#destroy"
  resources :users, except: [:new]
  resources :materials do 
    member do 
      patch :check_in
    end
  end
  resources :material_stocks
  resources :logs, only: :index
end
